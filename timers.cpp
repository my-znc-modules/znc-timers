#include <vector>
#include <string>
#include <ctime>
#include <cmath>
#include <functional>
#include <znc/main.h>
#include <znc/Modules.h>
#include <znc/IRCNetwork.h>
#include <znc/Chan.h>
#include <znc/User.h>

const std::string FORMAT_TIME = "%Y/%m/%d %H:%M:%S";
const std::string FORMAT_HOURS = "%H:%M:%S";

class MyMap : public MCString {
private:

    MyMap() {

    }

    ~MyMap() {
    }

protected:


public:

    //CONSTRUCTORS AND DESCTRUCTORS FOR SINGLETON

    static MyMap& GetInstance() {
        static MyMap instance;
        return instance;
    }
    MyMap(MyMap const&) = delete;
    void operator=(MyMap const&) = delete;

};

class CTimerInterval {
protected:
    std::time_t m_tStart;
    std::time_t m_tEnd;
    std::vector<CTimerInterval*> m_vPauses;

    CString ToStringRec(int depth) {
        CString indent = "";
        for (int i = 0; i < depth; i++) {
            indent += "\t";
        }
        CString res = indent + "Début intervalle : " + CString(m_tStart) + "\n";
        res += indent + "Fin intervalle : " + CString(m_tEnd) + "\n";
        if (m_vPauses.size() > 0) {
            res += indent + "Pauses : \n";
            for (CTimerInterval* interval : m_vPauses) {
                res += interval->ToStringRec(depth + 1) + "\n";
            }
        }
        return res;
    }

public:

    CTimerInterval() {
        Start();
    }

    CTimerInterval(int iTime) {
        Start();
        m_tEnd = m_tStart + iTime;
    }

    ~CTimerInterval() {
        std::vector<CTimerInterval*>::iterator it;
        for (it = m_vPauses.begin(); it != m_vPauses.end(); it++) {
            delete (*it);
        }
        m_vPauses.clear();
    }

    void Start() {
        time(&m_tStart);
    }

    void Stop() {
        time(&m_tEnd);
    }

    void Pause() {
        CTimerInterval* pPause = new CTimerInterval();
        m_vPauses.push_back(pPause);
    }

    void Resume() {
        CTimerInterval* pPause = m_vPauses.back();
        pPause->Stop();
        //for pauses in countdowns
        m_tEnd = m_tEnd + pPause->GetDuration() - difftime(pPause->m_tStart, m_tStart);
    }

    std::time_t GetStartTime() {
        return m_tStart;
    }

    std::time_t GetEndTime() {
        return m_tEnd;
    }

    double GetDuration() {
        double timeDiff = difftime(m_tEnd, m_tStart);
        double timePauses = 0.0;
        for (CTimerInterval* interval : m_vPauses) {
            timePauses += interval->GetDuration();
        }
        return timeDiff - timePauses;
    }

    double GetNormalDuration() {
        return difftime(m_tEnd, m_tStart);
    }

    CString ToString() {
        return this->ToStringRec(0);
    }

};

class CMyTimer {
protected:
    CUser* m_pUser;
    CString m_sName;
    CString m_sMessage;
    std::vector<CTimerInterval*> m_vIntervals;

    void EndCurrentInterval() {
        if (!m_vIntervals.empty()) {
            CTimerInterval* interval = m_vIntervals.back();
            interval->Stop();
        }
    }

    void ClearIntervals() {
        std::vector<CTimerInterval*>::iterator it;
        for (it = m_vIntervals.begin(); it != m_vIntervals.end(); it++) {
            delete (*it);
        }
        m_vIntervals.clear();
    }

public:

    CMyTimer(CUser* pUser, CString sName, CString sMessage, bool bStart) : m_pUser(pUser), m_sName(sName), m_sMessage(sMessage) {
        if (bStart) {
            Start();
        }
    }

    ~CMyTimer() {
        ClearIntervals();
    }

    void Start() {
        ClearIntervals();
        CreateInterval();
    }

    void Stop() {
        EndCurrentInterval();
    }

    void CreateInterval() {
        EndCurrentInterval();
        CTimerInterval* newInterval = new CTimerInterval();
        m_vIntervals.push_back(newInterval);
    }

    void Pause() {
        if (!m_vIntervals.empty()) {
            m_vIntervals.back()->Pause();
        }
    }

    void Resume() {
        if (!m_vIntervals.empty()) {
            m_vIntervals.back()->Resume();
        }
    }

    CString GetName() {
        return m_sName;
    }

    double GetDuration() {
        double duration = 0.0;
        for (CTimerInterval* interval : m_vIntervals) {
            duration += interval->GetDuration();
        }
        return duration;
    }

    double GetCurrentDuration() {
        double duration = 0.0;
        return duration;
    }

    CString GetNamedFormat(CString sMessage = "") {
        if (sMessage.empty()) {
            sMessage = m_sMessage;
        }
        unsigned int size = m_vIntervals.size();
        if (size > 0) {
            CTimerInterval* tiStart = m_vIntervals.front();
            std::time_t tStart = tiStart->GetStartTime();
            CTimerInterval* tiEnd = m_vIntervals.back();
            std::time_t tEnd = tiEnd->GetEndTime();

            CString sDiff = CString::ToTimeStr(GetDuration());
            MyMap::GetInstance()["NAME"] = m_sName;
            MyMap::GetInstance()["START"] = CUtils::FormatTime(tStart, FORMAT_TIME, m_pUser->GetTimezone());
            MyMap::GetInstance()["END"] = CUtils::FormatTime(tEnd, FORMAT_TIME, m_pUser->GetTimezone());
            MyMap::GetInstance()["DIFF"] = sDiff;
            MyMap::GetInstance()["NB_INTERVALS"] = CString(size);

            return CString::NamedFormat(sMessage, MyMap::GetInstance());
        }
        return CString("Timer not started !");
    }

};

class CCountdown {
protected:
    CString m_sName;
    CString m_sMessage;
    CTimerInterval* m_pInterval;
    int m_iSecondsToWait;

public:

    CCountdown(CString sName, int iSecondsToWait, CString sMessage, bool bStart)
    : m_sName(sName), m_sMessage(sMessage), m_iSecondsToWait(iSecondsToWait) {
        m_pInterval = nullptr;
    }

    ~CCountdown() {
        if (nullptr != m_pInterval) {
            delete m_pInterval;
        }
    }

    void Start(int iSecondsToWait) {
        if (m_pInterval != nullptr) {
            delete m_pInterval;
            m_pInterval = nullptr;
        }
        m_pInterval = new CTimerInterval(iSecondsToWait);
    }

    void Start() {
        Start(m_iSecondsToWait);
    }

    void Pause() {
        if (nullptr != m_pInterval) {
            m_pInterval->Pause();
        }
    }

    void Resume() {
        if (nullptr != m_pInterval) {
            m_pInterval->Resume();
        }
    }

    CString ToString() {
        if (nullptr != m_pInterval) {
            return m_pInterval->ToString();
        }
        return "";
    }

    CString GetName() {
        return m_sName;
    }

    double GetDuration() {
        if (nullptr != m_pInterval) {
            return m_pInterval->GetDuration();
        }
        return -1.0;
    }

};

class CAlarm {
protected:
    CString m_sName;
    CString m_sMessage;
    std::time_t m_tActivationTime;
    double m_dSecondsToWait;
    CCountdown m_countdown;

public:

    CAlarm(CString sName, CString sMessage, std::time_t tActivationTime) :
    m_sName(sName), m_sMessage(sMessage), m_tActivationTime(tActivationTime),
    m_countdown(m_sName, 0, m_sMessage, false) {
        
    }

    ~CAlarm() {

    }

    void Start() {
        m_dSecondsToWait = difftime(m_tActivationTime, time(nullptr));
        m_countdown.Start(m_dSecondsToWait);
    }
    
    double GetDouble() {
        return m_dSecondsToWait;
    }

};

class CWaitJob : public CModuleJob {
protected:
    CCountdown* m_pCountdown;

public:

    CWaitJob(CModule* pModule, CCountdown* interval) : CModuleJob(pModule, "Countdown_" + interval->GetName(), "Just to wait"), m_pCountdown(interval) {

    }

    virtual void runThread() {
        int time = static_cast<int> (m_pCountdown->GetDuration());
        for (int i = 0; i < time; i++) {
            if (wasCancelled()) {
                return;
            }
            sleep(1);
        }
    }

    virtual void runMain() {
        GetModule()->PutModule("test lol : " + CString(m_pCountdown->GetDuration()));
        //        GetModule()->PutModule("Total duration : " + CString(m_pCountdown->GetNormalDuration()));
        GetModule()->PutModule("Countdown :\n" + m_pCountdown->ToString());
    }

};

class CTimersMod : public CModule {
protected:
    std::map<CString, CMyTimer*> m_mTimers;
    std::map<CString, CCountdown*> m_mCountdowns;
    std::map<CString, CAlarm*> m_mAlarms;

    CMyTimer* GetTimer(const CString& sName) {
        CMyTimer* pTimer = nullptr;
        try {
            pTimer = m_mTimers.at(sName);
        }
        catch (std::out_of_range oor) {
            PutModule("Timer " + sName + " not found.");
        }
        return pTimer;
    }

    void CreateTimer(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        CString sMessage = sCommand.Token(2);
        sMessage = "{NAME} started at {START} and stopped at {END}, so duration "
                "is {DIFF} with {NB_INTERVALS} interval(s).";
        CString sStart = sCommand.Token(3);
        bool bStart = (sStart.Equals("true", CString::CaseInsensitive)
                || sStart.Equals("1", CString::CaseInsensitive)
                || sStart.Equals("start", CString::CaseInsensitive));
        CMyTimer* pTimer = new CMyTimer(GetUser(), sName, sMessage, bStart);
        m_mTimers.insert(std::make_pair(sName, pTimer));
        PutModule("Timer " + sName + " created.");
    }

    void DeleteTimer(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        std::map<CString, CMyTimer*>::iterator it = m_mTimers.find(sName);
        if (it != m_mTimers.end()) {
            it->second->Stop();
            delete it->second;
            m_mTimers.erase(it);
            PutModule("Timer " + sName + " deleted.");
        }
        else {
            PutModule("Timer " + sName + " not found.");
        }
    }

    void ExecuteTimerCommand(const CString& sCommand, std::function<void(CMyTimer*) > timerCommand,
            CString sMessage) {
        CString sName = sCommand.Token(1);
        CMyTimer* pTimer = GetTimer(sName);
        if (nullptr != pTimer) {
            timerCommand(pTimer);
            PutModule(pTimer->GetNamedFormat(sMessage));
        }
    }

    void StartTimer(const CString& sCommand) {
        CString sMessage = "Timer {NAME} started at {START}.";
        ExecuteTimerCommand(sCommand, &CMyTimer::Start, sMessage);
    }

    void IntervalTimer(const CString& sCommand) {
        CString sMessage = "Interval for timer {NAME} created, there are {NB_INTERVALS} for this timer.";
        ExecuteTimerCommand(sCommand, &CMyTimer::CreateInterval, sMessage);
    }

    void StopTimer(const CString& sCommand) {
        CString sMessage = "Timer {NAME} stopped at {END} for a duration of {DIFF}.";
        ExecuteTimerCommand(sCommand, &CMyTimer::Stop, sMessage);
    }

    void PauseTimer(const CString& sCommand) {
        CString sMessage = "Timer {NAME} paused.";
        ExecuteTimerCommand(sCommand, &CMyTimer::Pause, sMessage);
    }

    void ResumeTimer(const CString& sCommand) {
        CString sMessage = "Timer {NAME} resumed.";
        ExecuteTimerCommand(sCommand, &CMyTimer::Resume, sMessage);
    }

    void InfoTimer(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        CMyTimer* pTimer = m_mTimers.at(sName);
        if (nullptr != pTimer) {
            CString sMessage = pTimer->GetNamedFormat();
            PutModule(sMessage);
        }
    }

    void ListMyTimers(const CString& sCommand) {
        CString sTimers = "Your timers : ";
        std::map<CString, CMyTimer*>::iterator it;
        for (it = m_mTimers.begin(); it != m_mTimers.end(); it++) {
            sTimers.append(it->second->GetName());
            if (it != std::prev(m_mTimers.end())) {
                sTimers.append(", ");
            }
        }
        PutModule(sTimers);
    }

    void CreateCountdown(const CString& sCommand) {
        VCString vsArgs;
        sCommand.Split(" ", vsArgs, false, "\"", "\"", true, true);
        try {
            CString sName = vsArgs.at(1); //sCommand.Token(1);
            int iSeconds = vsArgs.at(2).ToInt(); //sCommand.Token(2).ToDouble();
            CString sMessage = sCommand.Token(3); //vsArgs.at(3);
            sMessage = "{NAME} started at {START} and stpped at {END}, so duration "
                    "is {DIFF} with {NB_INTERVALS} interval(s).";
            CString sStart = sCommand.Token(4); //vsArgs.at(4);
            bool bStart = (sStart.Equals("true", CString::CaseInsensitive)
                    || sStart.Equals("1", CString::CaseInsensitive)
                    || sStart.Equals("start", CString::CaseInsensitive));
            CCountdown *pCountdown = new CCountdown(sName, iSeconds, sMessage, bStart);
            auto added = m_mCountdowns.insert(std::make_pair(sName, pCountdown));
            if (added.second) {
                CString notif = "Countdown " + sName + " created";
                if (bStart) {
                    notif += " and started.";
                    pCountdown->Start();
                    AddJob(new CWaitJob(this, pCountdown));
                }
                else {
                    notif += ".";
                }
                PutModule(notif);
            }
        }
        catch (const std::out_of_range oor) {
            PutModule("Too few arguments.");
        }
    }

    void StartCountdown(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        int iTimeToWait = sCommand.Token(2).ToInt();
        try {
            CCountdown* pCountdown = m_mCountdowns.at(sName);
            if (iTimeToWait) {
                pCountdown->Start(iTimeToWait);
            }
            else {
                pCountdown->Start();
            }
            AddJob(new CWaitJob(this, pCountdown));
            PutModule("Countdown " + sName + " started.");
        }
        catch (std::out_of_range oor) {
            PutModule("Countdown " + sName + " not found.");
        }
    }

    void PauseCountdown(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        try {
            CCountdown* pCountdown = m_mCountdowns.at(sName);
            pCountdown->Pause();
            CancelJob("Countdown_" + sName);
            PutModule("Countdown " + sName + " paused.");
        }
        catch (std::out_of_range oor) {
            PutModule("Countdown " + sName + " not found.");
        }
    }

    void ResumeCountdown(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        try {
            CCountdown* pCountdown = m_mCountdowns.at(sName);
            pCountdown->Resume();
            AddJob(new CWaitJob(this, pCountdown));
            PutModule("Countdown " + sName + " resumed.");
        }
        catch (std::out_of_range oor) {
            PutModule("Countdown " + sName + " not found.");
        }
    }

    void ResetCountdown(const CString& sCommand) {

    }
    
    void CreateAlarm(const CString& sCommand) {
        CString sName = sCommand.Token(1);
        PutModule(sName);
        CString sHours = sCommand.Token(2);
        PutModule(sHours);
        CString sMessage = sCommand.Token(3);
        PutModule(sMessage);
        struct tm time;
        strptime(sHours.c_str(), FORMAT_HOURS.c_str(), localtime(&time));
        std::time_t timeT = mktime(&time);
//        PutModule(CUtils::FormatTime(timeT, FORMAT_HOURS, GetUser()->GetTimezone()));
        CAlarm alarm = CAlarm(sName, sMessage, timeT);
        alarm.Start();
        PutModule(CString(alarm.GetDouble()));
        PutModule("Alarm started");
    }

public:

    MODCONSTRUCTOR(CTimersMod) {
        AddHelpCommand();
        //COMMANDS FOR TIMERS
        AddCommand("createTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::CreateTimer), "<name> <message> [<start>]", "Create a timer.");
        AddCommand("deleteTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::DeleteTimer), "<name>", "Delete a timer.");
        AddCommand("startTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::StartTimer), "<name>", "Start a timer.");
        AddCommand("intervalTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::IntervalTimer), "<name>", "Create an interval for a timer.");
        AddCommand("stopTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::StopTimer), "<name>", "Stop a timer.");
        AddCommand("pauseTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::PauseTimer), "<name>", "Pause a timer.");
        AddCommand("ResumeTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::ResumeTimer), "<name>", "Resume a timer.");
        AddCommand("infoTimer", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::InfoTimer), "<name>", "Show informations of a timer.");
        AddCommand("listTimers", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::ListMyTimers), "", "List timers.");
        AddCommand("createCountdown", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::CreateCountdown), "<name> <time> <message> [<start>]", "Create countdown");
        AddCommand("startCountdown", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::StartCountdown), "<name>", "Start a countdown.");
        AddCommand("pauseCountdown", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::PauseCountdown), "<name>", "Pause countdown");
        AddCommand("ResumeCountdown", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::ResumeCountdown), "<name>", "Resumes countdown");
        AddCommand("CreateAlarm", static_cast<CModCommand::ModCmdFunc> (&CTimersMod::CreateAlarm), "<name> <time> <message>", "Create alarm");
    }

    virtual ~CTimersMod() {
        std::map<CString, CMyTimer*>::iterator it;
        for (it = m_mTimers.begin(); it != m_mTimers.end(); it++) {
            delete it->second;
        }
        m_mTimers.clear();
        std::map<CString, CCountdown*>::iterator itCountdown;
        for (itCountdown = m_mCountdowns.begin(); itCountdown != m_mCountdowns.end(); itCountdown++) {
            delete itCountdown->second;
        }
        m_mCountdowns.clear();
    }

};

NETWORKMODULEDEFS(CTimersMod, "Module to time things using commands.")
