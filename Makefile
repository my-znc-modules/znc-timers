MODULES_DIR = /var/lib/znc/modules

all: timers.so
	
	
timers.so: timers.cpp
	znc-buildmod $?

copy: timers.so
	cp $< $(MODULES_DIR)

.PHONY: clean
clean:
	rm -f timers.so